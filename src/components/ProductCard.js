import {Card, Button, Col, Row, Container} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom';
import Table from 'react-bootstrap/Table';
import '../App.css';
import Placeholder from 'react-bootstrap/Placeholder';

export default function ProductCard ({productProps}) {

	console.log(productProps);
	console.log(typeof productProps);

	const {name, description, price, _id, productName, availableInStock} = productProps;

	return(

		<Col xs={12} sm={6} md={4} lg={3} className="mb-3">
		      <Card style={{ width: '18rem', height: '27rem' }} className="fixed-height">
		        <Card.Img variant="top" src="https://psediting.websites.co.in/obaju-turquoise/img/product-placeholder.png" />

		        <Card.Body>
		          <Card.Title>{productName}</Card.Title>


				<Card.Text>
		            $ {price}
		          </Card.Text>

		          <Link className="btn btn-primary" to={`/courseView/${_id}`}> Details </Link>
		        </Card.Body>
		      </Card>
		</Col>


		)


}
