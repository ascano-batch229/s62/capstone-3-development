
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import { Form, Button, Modal } from "react-bootstrap";

export default function AddProduct() {
  const [productName, setProductname] = useState("");
  const [description, setDescription] = useState("");
  const [availableInStock, setAvailableInStock] = useState("");
  const [price, setPrice] = useState("");

  const [isActive, setIsActive] = useState(false);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    // Validation to enable register button when all fields are populated and both password match
    if (
      productName !== "" &&
      description !== "" &&
      availableInStock !== "" &&
      price !== ""
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productName, description, availableInStock, price]);

  function productAdd(e) {
    // e.preventDefault();

    fetch("http://localhost:4000/products/NewProduct", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productName: productName,
        description: description,
        availableInStock: availableInStock,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Product Successfully Added!",
            icon: "success",
            text: "Product Successfully added to the shop",
          });

          setProductname("");
          setDescription("");
          setAvailableInStock("");
          setPrice("");
          setShowModal(false);
        } else {
          Swal.fire({
            title: "Unable to add product",
            icon: "error",
            text: "Check your details and try again!",
          });
        }
      });
  }

  return (
    <>
      <Button variant="danger" onClick={() => setShowModal(true)}>
        Add Product
      </Button>

      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Add new Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>

          <Form onSubmit={(e) => productAdd(e)}>
            <Form.Group className="m-3">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="productName"
                placeholder="Product Name"
                value={productName}
                onChange={(e) => setProductname(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="m-3">
				<Form.Label>Description</Form.Label>
				<Form.Control
				type="description"
				placeholder="Description"
				value={description}
				onChange={(e) => setDescription(e.target.value)}
				required
				/>
			</Form.Group>

		<Form.Group className="m-3">
          <Form.Label>Available In Stock</Form.Label>
          <Form.Control
            type="number"
            placeholder="Available In Stock"
            value={availableInStock}
            onChange={(e) => setAvailableInStock(e.target.value)}
            required
          />
        </Form.Group>



        <Form.Group className="m-3">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            placeholder="Price"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>

        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowModal(false)}>
            Close
          </Button> 
          <Button variant="primary" type="submit" disabled={!isActive}>
            Add Product
          </Button>
        </Modal.Footer>
      </Form>
    </Modal.Body>
  </Modal>
</>

);
}
