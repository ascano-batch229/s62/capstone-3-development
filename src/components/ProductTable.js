import Table from 'react-bootstrap/Table';
import {Button, Modal} from 'react-bootstrap';
import '../App.css';
import {useState, useParams, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';




export default function ProductTable({products}){

const navigate = useNavigate();

  const [show, setShow] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState({});
  const [productName, setProductname] = useState("");
  const [description, setDescription] = useState("");
  const [availableInStock, setAvailableInStock] = useState(0);
  const [price, setPrice] = useState(0);
  const [token, setToken] = useState("");


  const handleClose = () => setShow(false);

  const handleShow = (product) => {
    setSelectedProduct(product);
    setProductname(product.productName);
    setDescription(product.description);
    setAvailableInStock(product.availableInStock);
    setPrice(product.price);
    setShow(true);
  }

  const handleSubmit = (e) => {
    // e.preventDefault();
    SaveChanges();
    handleClose();
  }

useEffect(() => {
  setToken(localStorage.getItem("token"));
  
}, []);

function SaveChanges() {

  const updatedProduct = {
    productName: productName,
    description: description,
    availableInStock: availableInStock,
    price: price
  };

  fetch(`http://localhost:4000/products/${selectedProduct._id}`, {
    method: 'PUT',
    headers:{
      'Content-Type' : 'application/json',
      'Authorization' : `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify(updatedProduct)
  })
  .then(response => response.json())
  .then(data => {
    console.log(data);
    setSelectedProduct(data);
 

if(data === true){
      Swal.fire({
        title: "Product Updated!",
        icon: "success",
        text: "Product Updated successfully"
      });

      navigate("/dashboard")
    } else {
      Swal.fire({
        title: "Unable to add product",
        icon: "error",
        text: "Check details and try again!"
      });
    }
  })
  .catch(error => {
    console.error('Error:', error);
  });
};

function Archive(product) {
  

  const archiveProduct = {
    "isActive" :  false
  };

  fetch(`http://localhost:4000/products/${selectedProduct._id}/archive`, {
    method: 'PUT',
    headers:{
      'Content-Type' : 'application/json',
      'Authorization' :`Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify(archiveProduct),

  })
  .then(response => response.json())
  .then(data => {
    console.log(data);
    setSelectedProduct(data);
 

if(data === true){
      Swal.fire({
        title: "Product Archived!",
        icon: "success",
        text: "Product Moved to Archived"
      });

      
    } else {
      Swal.fire({
        title: "Unable to add product",
        icon: "error",
        text: "Check details and try again!"
      });
    }
  })
  .catch(error => {
    console.error('Error:', error);
  });
};

 

	return(
    
    <>
      <Table striped bordered hover className="table">
        <thead>
          <tr>
            <th className="product-name">Product Name</th>
            <th className="product-description ">Product Description</th>
            <th className="in-stock">In Stock</th>
            <th className="price">Price</th>
            <th className="action"> Action </th>
          </tr>
        </thead>
        <tbody>

          {products.map(product => (
            <tr key={product._id}>
              <td className="product-name">{product.productName}</td>
              <td className="product-description">{product.description}</td>
              <td className="in-stock">{product.availableInStock}</td>
              <td className="price">{product.price}</td>
              <td className="action" > 
                <Button className="m-2 btn-size" variant="primary" onClick={() => handleShow(product)} block >Edit</Button> 
                <Button className="m-2 btn-size" variant="primary" onClick ={() => Archive(product)}block> Archive </Button>
              </td>
            </tr>
          ))}
        </tbody>
     </Table>

      <Modal show={show} onHide={handleClose} animation={false}>

        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          
            <form>

              <div className="form-group">
                <label htmlFor="productName">Product Name</label>
                <input type="text" className="form-control" id="productName" placeholder={selectedProduct.productName} value={productName} onChange={(e) => setProductname(e.target.value)} />
              </div>

              <div className="form-group">
                <label htmlFor="productDescription">Product Description</label>
                <textarea className="form-control" id="productDescription" rows="3" placeholder={selectedProduct.description} value={description} onChange={(e) => setDescription(e.target.value)}></textarea>
              </div>

               <div className="form-group">
                <label htmlFor="AvailableInStock"> In Stock </label>
                <input type="number" className="form-control" id="AvailableInStock" placeholder={selectedProduct.availableInStock} value={availableInStock} onChange={(e) => setAvailableInStock(e.target.value)}/>
              </div>             

              <div className="form-group">
                <label htmlFor="productPrice">Product Price</label>
                <input type="number" className="form-control" id="productPrice" placeholder={selectedProduct.price} value={price} onChange={(e) => setPrice(e.target.value)} />
              </div>

            </form>


        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>

          <Button variant="primary" onClick={handleSubmit}>
            Save Changes
          </Button>

        </Modal.Footer>

      </Modal>     



    </>



 
      
 )
		

}