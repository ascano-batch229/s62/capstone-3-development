import UserContext from '../UserContext.js'
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import { Form, Button, Modal } from "react-bootstrap";
import ProductTable from '../components/ProductTable.js'

export default function EditProduct({ productToEdit }) {
  const [productName, setProductname] = useState(productToEdit.productName);
  const [description, setDescription] = useState(productToEdit.description);
  const [availableInStock, setAvailableInStock] = useState(productToEdit.availableInStock);
  const [price, setPrice] = useState(productToEdit.price);

  const [isActive, setIsActive] = useState(false);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    // Validation to enable register button when all fields are populated and both password match
    if (
      productName !== "" &&
      description !== "" &&
      availableInStock !== "" &&
      price !== ""
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productName, description, availableInStock, price]);

  function productEdit(e) {
    e.preventDefault();

    fetch(`http://localhost:4000/products/${productToEdit._id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productName: productName,
        description: description,
        availableInStock: availableInStock,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Product Successfully Edited!",
            icon: "success",
            text: "Product information has been updated",
          });

          setProductname("");
          setDescription("");
          setAvailableInStock("");
          setPrice("");
          setShowModal(false);
        } else {
          Swal.fire({
            title: "Unable to edit product",
            icon: "error",
            text: "Check your details and try again!",
          });
        }
      });
  }

  return (
  <>
    <Button variant="danger" onClick={() => setShowModal(true)}>
      Edit Product
    </Button>

    <Modal show={showModal} onHide={() => setShowModal(false)}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Product</Modal.Title>
      </Modal.Header>
      <Modal.Body>

        <Form onSubmit={(e) => productEdit(e)}>

          <Form.Group className="m-3">
            <Form.Label>Product Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter product name"
              value={productName}
              onChange={(e) => setProductname(e.target.value)}
            />
          </Form.Group>

       

          <Form.Group className="m-3">
            <Form.Label>Description</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter product name"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="m-3">
            <Form.Label>In Stock</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter product name"
              value={availableInStock}
              onChange={(e) => setAvailableInStock(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="m-3">
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter product name"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </Form.Group>


        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => setShowModal(false)}>
          Close
        </Button>
        <Button variant="primary" type="submit" onClick={(e) => productEdit(e)}>
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>

    <ProductTable productEdit={productEdit} />
  </>
);

}