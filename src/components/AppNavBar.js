import {useContext} from 'react';
import NavBar from 'react-bootstrap/NavBar';
import Nav from 'react-bootstrap/Nav';
import { NavLink} from 'react-router-dom';
import UserContext from '../UserContext.js'



export default function	AppNavBar(){

	// const[user, setUser] =useState(localStorage.getItem('email'));

	const {user} = useContext(UserContext);

	return (
		<NavBar bg="light" className="p-3 text-center">
			<NavBar.Brand as={NavLink} exact to="/"> CAPSTONE </NavBar.Brand>
			<NavBar.Toggle aria-controls="basic-navbar-nav"/> 
			<NavBar.Collapse id="basic-navbar-nav">	
				<Nav className="ml-auto ">
					<Nav.Link as={NavLink} exact to="/"> Home </Nav.Link>
					<Nav.Link as={NavLink} exact to="/courses"> Products</Nav.Link>
					<Nav.Link as={NavLink} exact to="/products"> Products2</Nav.Link>

					
					{
										
						(user.id !== null && user.isAdmin) ? (
						    <>
						        {/*<Nav.Link as={NavLink} exact to="/dashboard"> Dashboard </Nav.Link>*/}

						    	<Nav.Link as={NavLink} exact to="/admin"> Admin Panel </Nav.Link>
						        <Nav.Link as={NavLink} exact to="/logout"> Logout </Nav.Link>
						    </>
						) : (user.id !== null && !user.isAdmin) ? (
						    <>
						    	<Nav.Link as={NavLink} exact to="/profile"> Profile </Nav.Link>						
						        <Nav.Link as={NavLink} exact to="/logout"> Logout </Nav.Link>
						        

						    </>
						) : (
						    <>
						        <Nav.Link as={NavLink} exact to="/login"> Login </Nav.Link>
						        <Nav.Link as={NavLink} exact to="/register"> Register </Nav.Link>
						    </>
						)
					}


				</Nav>

			</NavBar.Collapse>
		</NavBar>


		)


      
}