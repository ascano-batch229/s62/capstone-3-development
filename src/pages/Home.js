import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'
import CourseCard from '../components/CourseCard.js'
import {Card, Button, Col, Row, Container} from 'react-bootstrap'



export default function Home(){
	return(

		<>
    		<Banner/>
    		<Highlights/>
    		
    	</>
		)
}