import { Form, Button, Radio } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js'
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Register(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// State Hooks - Store values of the input fields
	const [email, setEmail] = useState('');
	const [firstName, setFirstname] = useState('');
	const [lastName, setLastname] = useState('');
	const [userName, setUsername] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false)

	console.log(firstName);
	console.log(lastName);
	console.log(userName);
	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
		// Validation to enable register button when all fields are populated and both password match
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},  [email, password1, password2])

	// Function to simulate user registration

	function registerUser(e){

		// Prevents page redirection via form submission
		e.preventDefault();

		// fetch('http://localhost:4000/users/checkEmail', {
		// 	method: 'POST',
		// 	headers: {
		// 		'Content-Type' : 'application/json'
		// 	},
		// 	body: JSON.stringify({
		// 		email: email

		// 	})
		// })
		// .then(res => res.json())
		// .then(data => {
		// 	console.log(data)	

		// if(data === true){
		// 	Swal.fire({
		// 		title: "Email already exists",
		// 		icon: 'error',
		// 		text: "Please choose different email"
		// 	})

		// 	return;
		// }	


		fetch('http://localhost:4000/users/registration', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				userName: userName,
				email: email,
				password: password1,
				password2: password2
			})
			})
			.then(res => res.json())
			.then(data => {
			console.log(data)

			if(data === true){
			Swal.fire({
				title: "Registration Sucessfull!",
				icon: "success",
				text: "Welcome to the shop!"
			})	

			navigate("/login");
			}else{
			Swal.fire({
				title: "Registration Failed",
				icon: "error",
				text: "Check your details and try again!"			
				})

			}

		})
	



		setEmail('');
		setPassword1('');
		setPassword2('');


	}

	return(

		(user.id !== null ) ?

			<Navigate to="/login"/>
			:
	<div className="border border-dark">
		<Form onSubmit={(e) => registerUser(e)}>

			<Form.Group className="mb-3" controlId="userFirstname">
				<Form.Label> First Name</Form.Label>
				<Form.Control type="firstName" placeholder="First Name" value={firstName} onChange={e => setFirstname(e.target.value)} required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userLastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control type="lastName" placeholder="Last Name" value={lastName} onChange={e => setLastname(e.target.value)} required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userMobile">
				<Form.Label> Username </Form.Label>
				<Form.Control type="userName" placeholder="UserName" value={userName} onChange={e => setUsername(e.target.value)} required/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
				<Form.Text className="text-muted">
			 	 We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1" >
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Password"  value={password1} onChange={e => setPassword1(e.target.value)} required/>
			</Form.Group>


			<Form.Group className="mb-3" controlId="password2" >
				<Form.Label> Verify Password</Form.Label>
				<Form.Control type="password" placeholder="Verify Password"  value={password2} onChange={e => setPassword2(e.target.value)} required />
			</Form.Group>

			<Form.Group>
				<Form.Label> Please Select </Form.Label>
			</Form.Group>

			<Form.Group className="mb-3">
					<Form.Check inline type="radio" label="Personal Account" name="radioGroup" id="option1" /> 
					<Form.Check inline type="radio" label="Business Account" name="radioGroup" id="option2" />    
			</Form.Group>  

			

				{/*Conditional Rendering -> IF ACTIVE BUTTON IS CLICKABLE -> IF INACTIVE BUTTON IS NOT CLICKABLE*/}
				{
					(isActive)?
					<Button variant="primary" type="submit" controlId="submitBtn">
						Register
					</Button>
					:
					<Button variant="primary" type="submit" controlId="submitBtn" disabled>
						Register
					</Button>										

				}
		</Form>

	</div>
		) 


}