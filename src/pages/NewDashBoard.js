import {Container, Row, Col, Button, Table, Modal, Form} from 'react-bootstrap'
import Swal from 'sweetalert2';

import {useState, useEffect, useNavigate} from 'react';


export default function Panel(){

	const [selectedProduct, setSelectedProduct] = useState({});

	const [productId, setProductId] = useState("")
	const [productName, setProductname] = useState("");
 	const [description, setDescription] = useState("");
  	const [availableInStock, setAvailableInStock] = useState("");
  	const [price, setPrice] = useState("");

  	const [showAddModal, setShowAddModal] = useState(false);
  	const [showEditModal, setShowEditModal] = useState(false);
  	const [showArchiveModal, setShowArchiveModal] = useState(false);

  	const handleArchiveProduct = (id) => {

  	console.log(id);


	fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
    .then(res => res.json())
    .then(data => {	

    setProductId(data._id);
    setProductname(data.productName);
    setDescription(data.description);
    setAvailableInStock(data.availableInStock);
    setPrice(data.price);

  	console.log(data);

  	setShowArchiveModal(true);

  	})

	}

  	const handleAddProduct = () => setShowAddModal(true);

  	const handleEditProduct = (id) => {

  	console.log(id)


  	fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
    .then(res => res.json())
    .then(data => {	

    setProductId(data._id);
    setProductname(data.productName);
    setDescription(data.description);
    setAvailableInStock(data.availableInStock);
    setPrice(data.price);

  	console.log(data);

  	})

    //  setSelectedProduct(product);
    // setProductname(products.productName);
    // setDescription(products.description);
    // setAvailableInStock(products.availableInStock);
    // setPrice(products.price);
    setShowEditModal(true);
  }

  	const closeAddModal = () => setShowAddModal(false);

  	const closeEditModal = () => {

		// Clear input fields upon closing the modal
	    setProductname('');
	    setDescription('');
	    setPrice('');
	    

		setShowEditModal(false);
	};

	const closeArchiveModal = () => setShowArchiveModal(false)

	const [isActive, setIsActive] = useState(false);


	 const handleSubmit = (e) => {
	    e.preventDefault();
	    SaveChanges();

	    Archive();
	    setShowEditModal(false);
	    setShowArchiveModal(false);
	  }



  useEffect(() => {
    // Validation to enable register button when all fields are populated and both password match
    if (
      productName !== "" &&
      description !== "" &&
      availableInStock !== "" &&
      price !== ""
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productName, description, availableInStock, price]);

	const [products, setProducts] = useState([]);

		useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
        .then(res => res.json())
        .then(data => {

  		// const { _id, productName, description, price } = data;

		setProducts(data.map(product => ({
		_id: product._id,
		productName: product.productName,
		description: product.description,
    availableInStock: product.availableInStock,
		price: product.price,
		})));

  		console.log(data);

    	})	

	},[])


function productAdd(e) {
    e.preventDefault();

    fetch("http://localhost:4000/products/NewProduct", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productName: productName,
        description: description,
        availableInStock: availableInStock,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Product Successfully Added!",
            icon: "success",
            text: "Product Successfully added to the shop",
          });

          setProductname("");
          setDescription("");
          setAvailableInStock("");
          setPrice("");
          setShowAddModal(false);
        } else {
          Swal.fire({
            title: "Unable to add product",
            icon: "error",
            text: "Check your details and try again!",
          });
        }
      });
  }

  function SaveChanges() {


  const updatedProduct = {
    productName: productName,
    description: description,
    availableInStock: availableInStock,
    price: price
  };

  fetch(`http://localhost:4000/products/${productId}`, {
    method: 'PUT',
    headers:{
      'Content-Type' : 'application/json',
      'Authorization' : `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify(updatedProduct)
  })
  .then(response => response.json())
  .then(data => {
    console.log(data);
    setSelectedProduct(data);
 

if(data === true){
      Swal.fire({
        title: "Product Updated!",
        icon: "success",
        text: "Product Updated successfully"
      });

      
    } else {
      Swal.fire({
        title: "Unable to add product",
        icon: "error",
        text: "Check details and try again!"
      });
    }
  })
  .catch(error => {
    console.error('Error:', error);
  });
};

function Archive() {
  // setSelectedProduct(product);

  const archiveProduct = {
    "isActive" :  false
  };

  fetch(`http://localhost:4000/products/${productId}/archive`, {
    method: 'PUT',
    headers:{
      'Content-Type' : 'application/json',
      'Authorization' :`Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify(archiveProduct),

  })
  .then(response => response.json())
  .then(data => {
    console.log(data);
    setSelectedProduct(data);
 

if(data === true){
      Swal.fire({
        title: "Product Archived!",
        icon: "success",
        text: "Product Moved to Archived"
      });

      
    } else {
      Swal.fire({
        title: "Unable to add product",
        icon: "error",
        text: "Check details and try again!"
      });
    }
  })
  .catch(error => {
    console.error('Error:', error);
  });
};



	return(

	<>
			<Container >
				<Row>
					<Col md={3}> 
						<div className="border border-dark vh-100"> 

						<h2 className="text-center mt-5"> ADMIN SIDEBAR </h2>

						</div>
					</Col>

					<Col md={9}>

						<div className="border border-dark d-flex justify-content-center" style={{height : '75px'}}> 
							<div className="pt-4">
								ADMIN TRY
							</div>
						</div>

						<div className="border border-dark text-end my-2" style={{height : '75px'}}>

						 <Button variant="success" className="m-3 ml-auto" onClick={handleAddProduct}> ADD PRODUCT </Button>

						 </div >

						 <div className="border border-dark">
						 	
						 	<div>

						 	<Table striped bordered hover>

						      <thead>
						        <tr>
						          <th>#</th>
						          <th>Product Name</th>
						          <th>Description</th>
						          <th>In stock</th>
						          <th>Price</th>
						          <th> Action</th>
						        </tr>
						      </thead>

						      <tbody>						      
						              {products.map((product, index) => (
								          <tr key={product._id}>
								            <td>{index + 1}</td>
								            <td>{product.productName}</td>
								            <td>{product.description}</td>
								            <td>{product.availableInStock}</td>
								            <td>{product.price}</td>
								            <td className="d-flex justify-content-center"> 
								              <Button variant="primary" className="mx-2" style={{width: '75px'}} onClick={() => handleEditProduct(product._id)}> Edit </Button>
								              <Button variant="primary" onClick={() => handleArchiveProduct(product._id)}> Archive </Button>
								            </td>
								          </tr>
								        ))}
						      </tbody>

						    </Table>

						 	</div>

						 </div>


					</Col>
				</Row>
			</Container>


      <Modal show={showAddModal} onHide={() => setShowAddModal(false)}>

        <Modal.Header closeButton>
          <Modal.Title>Add new Product</Modal.Title>
        </Modal.Header>

        <Modal.Body>

          <Form onSubmit={(e) => productAdd(e)}>
            <Form.Group className="m-3">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="productName"
                placeholder="Product Name"
                value={productName}
                onChange={(e) => setProductname(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="m-3">
				<Form.Label>Description</Form.Label>
				<Form.Control
				type="description"
				placeholder="Description"
				value={description}
				onChange={(e) => setDescription(e.target.value)}
				required
				/>
			</Form.Group>

		<Form.Group className="m-3">
          <Form.Label>Available In Stock</Form.Label>
          <Form.Control
            type="number"
            placeholder="Available In Stock"
            value={availableInStock}
            onChange={(e) => setAvailableInStock(e.target.value)}
            required
          />
        </Form.Group>



        <Form.Group className="m-3">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            placeholder="Price"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>

        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowAddModal(false)}>
            Close
          </Button> 
          <Button variant="primary" type="submit" disabled={!isActive}>
            Add Product
          </Button>
        </Modal.Footer>
      </Form>
    </Modal.Body>
  </Modal>


<Modal show={showEditModal} onHide={closeEditModal} animation={false}>

        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          
            <form onSubmit={(e) => SaveChanges(e)}>

              <div className="form-group">
                <label htmlFor="productName">Product Name</label>
                <input type="text" className="form-control" id="productName" placeholder={productName} value={productName} onChange={(e) => setProductname(e.target.value)} />
              </div>

              <div className="form-group">
                <label htmlFor="productDescription">Product Description</label>
                <textarea className="form-control" id="productDescription" rows="3" placeholder={description} value={description} onChange={(e) => setDescription(e.target.value)}></textarea>
              </div>

               <div className="form-group">
                <label htmlFor="AvailableInStock"> In Stock </label>
                <input type="number" className="form-control" id="AvailableInStock" placeholder={availableInStock} value={availableInStock} onChange={(e) => setAvailableInStock(e.target.value)}/>
              </div>          

              <div className="form-group">
                <label htmlFor="productPrice">Product Price</label>
                <input type="number" className="form-control" id="productPrice" placeholder={price} value={price} onChange={(e) => setPrice(e.target.value)} />
              </div>

            </form>


        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={closeEditModal}>
            Close
          </Button>

          <Button variant="primary" onClick={handleSubmit}>
            Save Changes
          </Button>

        </Modal.Footer>

      </Modal>     

		
      <Modal show={showArchiveModal} onHide={() => setShowArchiveModal(false)} onSubmit={(e) => Archive(e)}>

      
        <Modal.Header closeButton>
          <Modal.Title>Confirm Archive</Modal.Title>
        </Modal.Header>

        <Modal.Body>Are you sure you want to Archive the Selected Product?</Modal.Body>

        <Modal.Footer>

          <Button variant="secondary" onClick={closeArchiveModal}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSubmit}>
            Archive
          </Button>


        </Modal.Footer>
        
      </Modal>















	</>	
		


		)

}