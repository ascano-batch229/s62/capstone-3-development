import ProductCard from '../components/ProductCard.js'

import {useEffect, useState} from 'react';
import {Card, Button, Col, Row, Container} from 'react-bootstrap'

export default function Products(){

	const [products, setProducts] = useState([])

		useEffect(() => {

			fetch(`${process.env.REACT_APP_API_URL}/products/active`)
        		.then(res => res.json())
        		.then(data => {

  				console.log(data)

  				setProducts(data.map(product => {
  					return (

  						<ProductCard key={products} productProps = {product}/>
  						)
  				}))
    			})	
		},[])


		return(

			<>
				<Container>
					<Row>
						<Col>
							<div className="d-flex justify-content-center m-3">
								<h2> Product Page 2 </h2>
							</div>
						</Col>
					</Row>
				</Container>
				
				<Container>
					<Row>
						<Col>
							<div className="d-flex justify-content-center m-3">
								{products}
							</div>
						</Col>
					</Row>
				</Container>

			</>




			)

}