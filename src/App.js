import {useState, useEffect} from 'react';
import './App.css';
//AppNavBar importation
import AppNavBar from './components/AppNavBar.js'
import CourseView from './components/CourseView.js'
import CreateProduct from './components/CreateProduct.js'

import {Container} from 'react-bootstrap'
import{BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import Home from './pages/Home.js'
import Courses from './pages/Courses.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Logout from './pages/Logout.js'
import NotFound from './pages/NotFound.js'
import Dashboard from './pages/Dashboard.js'
import {UserProvider} from './UserContext.js'
import Panel from './pages/NewDashBoard.js'
import Products from './pages/Products.js'
// React JS is a single page application (SPA)

      // Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
      // When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
      // It renders the component executing the function component and it's expressions
      // After rendering it mounts the component displaying the elements
      // Whenever a state is updated or changes are made with React JS, it rerenders the component
      // Lastly, when a different page is loaded, it unmounts the component and repeats this process
      // The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page

function App() {

  const [user, setUser] = useState({

    /*
      SYNTAX: 
        localStorage.getItem(propertyName)
    */
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

// Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {

    console.log(user);
    console.log(localStorage);

  })


  return (
    // Fragment "<> and </>"
  /*We are mounting our components and to prepare output rendering*/
  <UserProvider value={{user, setUser, unsetUser}}>
    <>
      <Router>
          <AppNavBar/>
            <Container>
              <Routes>

                  <Route exact path="/" element={<Home/>}/>
                  <Route exact path="/courses" element={<Courses/>}/>

                  <Route exact path="/products" element={<Products/>}/>

                  <Route exact path="/login" element={<Login/>}/>
                  <Route exact path="/register" element={<Register/>}/>
                  <Route exact path="/logout" element={<Logout/>}/>
                  {/*<Route exact path="/dashboard" element={<Dashboard/>}/>*/}

                  <Route exact path="/admin" element={<Panel/>}/>
                  <Route exact path="/courseView/:courseId" element={<CourseView/>}/>
                  


                  <Route exact path="*" element={<NotFound/>}/>
              </Routes>
            </Container>
      </Router>
    </>
  </UserProvider>


  );
}

export default App;
